package com.happybit.happyfrog;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class MyIntentService extends Service implements SensorEventListener, TextToSpeech.OnInitListener {

    private float lastX, lastY, lastZ;

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private float deltaX = 0;
    private float deltaY = 0;
    private float deltaZ = 0;

    private float vibrateThreshold = 0;

    public Vibrator v;
    private boolean sensorTriggered = false;

    private TextToSpeech t1;
    private String TAG = "TTS";

    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private ValueEventListener postListener;

    private String userId;
    private ArrayList<Post> posts = new ArrayList<>();
    private ArrayList<String> postUids = new ArrayList<>();
    private Post randomlySelectedPost;
    private String randomlySelectedPostUid;

    //Strings to register to create intent filter for registering the receivers
    private static final String ACTION_STRING_SERVICE = "ToService";
    private static final String ACTION_STRING_RESET = "ResetTrigger";

    //Create a broadcast receiver
    private BroadcastReceiver serviceReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == ACTION_STRING_SERVICE) {
                sensorManager.registerListener(MyIntentService.this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            } else if (intent.getAction() == ACTION_STRING_RESET) {
                sensorTriggered = false;
            }
        }
    };

    public MyIntentService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {


        //register the receiver
        if (serviceReceiver != null) {
            //Create an intent filter to listen to the broadcast sent with the action "ACTION_STRING_SERVICE"
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_SERVICE);
            intentFilter.addAction(ACTION_STRING_RESET);
            //Map the intent filter to the receiver
            registerReceiver(serviceReceiver, intentFilter);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Service", "onDestroy");
        // Unregister the receiver
        unregisterReceiver(serviceReceiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "MyWakelockTag");
        wakeLock.acquire();

        startForeground(1, buildForegroundNotification("Happyfrog"));

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // email address
            userId = user.getUid();
            Log.d("UID", userId);
        }

        sensorTriggered = false;

        // Write a message to the database
        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference("posts").child(userId);

        postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                Iterable<DataSnapshot> postsSnapshot = dataSnapshot.getChildren();

                for (DataSnapshot postSnapshot : postsSnapshot) {
                    Post post = postSnapshot.getValue(Post.class);
                    postUids.add(postSnapshot.getKey());
                    posts.add(post);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };

        mDatabase.addValueEventListener(postListener);

        t1 = new TextToSpeech(this, this);

        // Sensor manager
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            // success! we have an accelerometer
            sensorManager.registerListener(MyIntentService.this, accelerometer, sensorManager.SENSOR_DELAY_NORMAL);
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            vibrateThreshold = accelerometer.getMaximumRange() / 4;
        } else {
            // fail! we dont have an accelerometer!
        }

        //initialize vibration
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        return START_STICKY;
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            t1.setOnUtteranceProgressListener(mProgressListener);
        }
    }

    private UtteranceProgressListener mProgressListener = new UtteranceProgressListener() {
        @Override
        public void onStart(String utteranceId) {
        } // Do nothing

        @Override
        public void onError(String utteranceId) {
        } // Do nothing.

        @Override
        public void onDone(String utteranceId) {

            Intent intent = new Intent(getBaseContext(), RatingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("post", randomlySelectedPost);
            intent.putExtra("postUid", randomlySelectedPostUid);
            getApplication().startActivity(intent);
        }
    };

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        // get the change of the x,y,z values of the accelerometer
        deltaX = Math.abs(lastX - event.values[0]);
        deltaY = Math.abs(lastY - event.values[1]);
        deltaZ = Math.abs(lastZ - event.values[2]);

        // if the change is below 2, it is just plain noise
        if (deltaX < 5)
            deltaX = 0;
        if (deltaY < 5)
            deltaY = 0;
        if (deltaZ < 5)
            deltaZ = 0;

        // set the last know values of x,y,z
        lastX = event.values[0];
        lastY = event.values[1];
        lastZ = event.values[2];

        if (posts.size() > 0) {
            vibrate();
        }
    }

    // if the change in the accelerometer value is big enough, then vibrate!
    public void vibrate() {
        if ((deltaX > vibrateThreshold) || (deltaY > vibrateThreshold) || (deltaZ > vibrateThreshold)) {
            v.vibrate(50);
            if (sensorTriggered == false) {
                sensorTriggered = true;
                sensorManager.unregisterListener(this);
                int postIndex = new Random().nextInt(posts.size());
                randomlySelectedPost = posts.get(postIndex);
                randomlySelectedPostUid = postUids.get(postIndex);
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
                t1.speak(randomlySelectedPost.text, TextToSpeech.QUEUE_FLUSH, map);
                Log.d("Speak", "speak");
            }
        }
    }

    public Notification buildForegroundNotification(String filename) {
        NotificationCompat.Builder b = new NotificationCompat.Builder(this);

        b.setOngoing(true)
                .setContentTitle("Happyfrog")
                .setContentText(filename)
                .setSmallIcon(R.drawable.frog18)
                .setTicker("Happyfrog");

        return (b.build());
    }
}
