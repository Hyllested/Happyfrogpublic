package com.happybit.happyfrog;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

public class MainActivity extends Activity {

    private static final String ACTION_STRING_SERVICE = "ToService";
    private static final String ACTION_STRING_RESET = "ResetTrigger";

    Handler handler = new Handler();

    final Runnable r = new Runnable() {
        public void run() {
            Intent serviceIntent = new Intent(ACTION_STRING_RESET);
            sendBroadcast(serviceIntent);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent serviceIntent = new Intent(ACTION_STRING_SERVICE);
        sendBroadcast(serviceIntent);
        handler.postDelayed(r, 1000);
    }
}

