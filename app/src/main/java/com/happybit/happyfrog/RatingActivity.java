package com.happybit.happyfrog;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class RatingActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private String userId;

    private Post randomlySelectedPost;
    private String postUid;

    private ImageButton imageButtonHappy;
    private ImageButton imageButtonNeutral;
    private ImageButton imageButtonSad;

    final private String HAPPY_RATING = "0";
    final private String NEUTRAL_RATING = "1";
    final private String SAD_RATING = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Intent intent = getIntent();
        randomlySelectedPost = (Post) intent.getSerializableExtra("post");
        postUid = intent.getStringExtra("postUid");

        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Log.d("TEXT", randomlySelectedPost.text);

        imageButtonHappy = (ImageButton) findViewById(R.id.imageButtonHappy);
        imageButtonNeutral = (ImageButton) findViewById(R.id.imageButtonNeutral);
        imageButtonSad = (ImageButton) findViewById(R.id.imageButtonSad);

        imageButtonHappy.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Date timestamp = new Date();
                randomlySelectedPost.ratings.put(timestamp.toString(), HAPPY_RATING);
                mDatabase = FirebaseDatabase.getInstance().getReference();

                mDatabase.child("posts").child(userId).child(postUid).child("ratings").child(timestamp.toString()).setValue(HAPPY_RATING);

                goToMain();
            }
        });

        imageButtonNeutral.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Date timestamp = new Date();
                randomlySelectedPost.ratings.put(timestamp.toString(), NEUTRAL_RATING);
                mDatabase = FirebaseDatabase.getInstance().getReference();

                mDatabase.child("posts").child(userId).child(postUid).child("ratings").child(timestamp.toString()).setValue(NEUTRAL_RATING);
                goToMain();
            }
        });

        imageButtonSad.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Date timestamp = new Date();
                randomlySelectedPost.ratings.put(timestamp.toString(), SAD_RATING);
                mDatabase = FirebaseDatabase.getInstance().getReference();

                mDatabase.child("posts").child(userId).child(postUid).child("ratings").child(timestamp.toString()).setValue(SAD_RATING);
                goToMain();
            }
        });
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
