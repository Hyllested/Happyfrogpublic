package com.happybit.happyfrog;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by helle on 28-03-2017.
 */

@IgnoreExtraProperties
@SuppressWarnings("serial")
public class Post implements Serializable {

    public HashMap<String, String> ratings = new HashMap<>();
    public String text;

    public Post() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Post(String text, HashMap<String, String> ratings) {
        this.ratings = ratings;
        this.text = text;
    }
}
